package org.io.api.mapper;

import org.io.api.dto.PeopleDto;
import org.io.api.entity.People;

import java.util.ArrayList;
import java.util.List;

public class PeopleMapper {
    public static List<PeopleDto> peopleListToDtoList(List<People> people) {
        List<PeopleDto> list = new ArrayList<>();
        people.forEach( item -> {
            PeopleDto peopleDto = new PeopleDto();
            peopleDto.setFirstName(item.getFirstName());
            peopleDto.setLdapuser(item.getLdapuser());
            peopleDto.setTitle(item.getTitle());
            peopleDto.setLastName(item.getLastName());
            list.add(peopleDto);
        });
        return list;
    }
}
