package org.io.api.service;

import java.io.InputStream;

public interface UploadService {
    public void upload(InputStream stream);
}
