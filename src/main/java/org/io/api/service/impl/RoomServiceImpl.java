package org.io.api.service.impl;

import org.io.api.entity.People;
import org.io.api.exception.PeopleException;
import org.io.api.repository.PeopleRepository;
import org.io.api.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    PeopleRepository repository ;

    public List<People> getPeopole(String room) {
        if(! isNumeric(room) || room.length() != 4) {
            throw new PeopleException(6, "Invalid room number");
        }
        List<People> list =  repository.findByRoom(Integer.parseInt(room));
        if(list.isEmpty()) {
            throw new PeopleException(5, "Room not found");
        } else {
            return list;
        }

    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
