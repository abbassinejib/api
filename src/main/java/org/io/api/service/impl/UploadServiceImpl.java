package org.io.api.service.impl;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.io.api.entity.People;
import org.io.api.exception.PeopleException;
import org.io.api.kafka.KafkaProduce;
import org.io.api.repository.PeopleRepository;
import org.io.api.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class UploadServiceImpl implements UploadService {

    @Autowired
    PeopleRepository repository ;

    @Autowired
    KafkaProduce kafkaProducer;


    public void upload(InputStream stream) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withIgnoreHeaderCase().withTrim());) {

            List peopleList = new ArrayList<>();
            List ldapList = new ArrayList<>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                ValidateRecords(csvRecord);
                String record = csvRecord.get(1);
                String[] records = record.split(" ");
                String title = null;
                String firsName = null ;
                String lastName = null ;
                String ldapuser = null;

                    if(records[0].contains(".")) {
                        title = records[0].replaceAll("\\.","");
                        firsName = records.length > 1 ? records[1] : null;
                        lastName =  records.length > 2 ? records[2] : null ;
                        ldapuser = records.length >  3 ? records[3].replaceAll("\\(","").replaceAll("\\)","") : null;
                    } else {
                        firsName =  records.length >  0 ? records[0] : null ;
                        lastName =  records.length >  1 ? records[1] : null;
                        ldapuser = records.length >  2 ? records[2].replaceAll("\\(","").replaceAll("\\)","") : null;
                    }
                if(ldapList.contains(ldapuser))    {
                    throw new PeopleException(3, "Duplicated people");
                }
                ldapList.add(ldapuser);

                People people =  new  People( ldapuser, title , firsName, lastName, Integer.parseInt(csvRecord.get(0)));
                peopleList.add(people);

            }
            repository.saveAll(peopleList);
            if(! peopleList.isEmpty()) {
                peopleList.forEach(item -> {
                    kafkaProducer.sendMessage((People) item);
                });
            }

        }
        catch(PeopleException e) {
            throw new PeopleException(e.getCode(), e.getMessage());
        }
        catch (IOException e) {
            throw new PeopleException(4, e.getMessage());
        }
    }

    private static void ValidateRecords(CSVRecord csvRecord) {
        if(csvRecord.get(0).isEmpty() || csvRecord.get(0).isBlank() || csvRecord.get(0) == null ) {
            throw new PeopleException(2, "Bad Request");
        }
    }
}
