package org.io.api.kafka;

import org.io.api.entity.People;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;


@Component
public class KafkaProduce {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(People people) {
        kafkaTemplate.send("peopleTopic", people.toString());
    }
}
