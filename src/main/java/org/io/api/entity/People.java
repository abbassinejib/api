package org.io.api.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "people")
public class People {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    private String ldapuser;
    private String title;
    private String firstName;

    private String middleName;
    private String lastName;
    private int room;

    public People( String ldapuser, String title, String firstName, String lastName, int room) {
        this.ldapuser = ldapuser;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.room = room;
    }

    public People() {
    }

    @Override
    public String toString() {
        return "People{" +
                "id=" + id +
                ", ldapuser='" + ldapuser + '\'' +
                ", title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", room=" + room +
                '}';
    }
}
