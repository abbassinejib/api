package org.io.api.controller;


import org.io.api.dto.RoomDto;
import org.io.api.dto.uploadDto;
import org.io.api.entity.People;
import org.io.api.mapper.PeopleMapper;
import org.io.api.service.RoomService;
import org.io.api.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    UploadService uploadService;

    @Autowired
    RoomService roomService;

    @PostMapping("/import")
    public ResponseEntity<uploadDto> uploadFile(
            @RequestParam("file") MultipartFile file)
            throws IOException {
            uploadService.upload(file.getInputStream());
            return new ResponseEntity<>(new uploadDto(0, "File uploded"), HttpStatus.OK);
    }

    @GetMapping("room/{number}")
    public ResponseEntity<RoomDto> getRoom(@PathVariable String number) {
        RoomDto room = new RoomDto();
        room.setRoom(number);
        room.setPeopple(PeopleMapper.peopleListToDtoList(roomService.getPeopole(number)));
        return new ResponseEntity<>(room, HttpStatus.OK);
    }

}


