package org.io.api.repository;

import org.io.api.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PeopleRepository extends JpaRepository<People, Integer> {


    List<People> findByRoom(int room);
}
