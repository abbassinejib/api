package org.io.api.exception;

import lombok.Data;

@Data
public class CustomErrorResponse {
    private int code;
    private String message;
}
