package org.io.api.exception;

import lombok.Data;

@Data
public class PeopleException extends RuntimeException {
    int code;
    public PeopleException(int code, String message) {
        super(message);
        this.code = code;
    }

}
