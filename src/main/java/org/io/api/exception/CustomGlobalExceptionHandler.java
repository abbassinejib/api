package org.io.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
    public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

        @ExceptionHandler(PeopleException.class)
        public ResponseEntity<CustomErrorResponse> customHandleNotFound(PeopleException ex, WebRequest request) {

            CustomErrorResponse errors = new CustomErrorResponse();
            errors.setMessage(ex.getMessage());
            errors.setCode(ex.getCode());
            if(ex.getCode() == 5) {
                return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
            }

        }


}

