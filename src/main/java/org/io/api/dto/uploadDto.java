package org.io.api.dto;

import lombok.Data;

@Data
public class uploadDto {
    int code ;
    String message;

    public uploadDto(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
