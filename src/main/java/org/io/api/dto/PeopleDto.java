package org.io.api.dto;

import lombok.Data;

@Data
public class PeopleDto {
    private String ldapuser;
    private String title;
    private String firstName;
    private String lastName;
}
