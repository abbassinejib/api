package org.io.api.dto;

import lombok.Data;

import java.util.List;

@Data
public class RoomDto {
    private String room;
    private List<PeopleDto> peopple;
}
